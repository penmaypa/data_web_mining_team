# DATA CLEANSING: ISEQ 20008 to 2018
#
# Dependencies: our_functions.r
source_dep <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/R/our_functions.r?inline=false"
source(source_dep)

data <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/Raw%20Data/iseq_2008-2018.csv?inline=false"

df <- read.csv(data)
raw <- df 

print(
  head(df[1:5,])
)

# Check for missing values0
result <- check_invalid_values(df)
print(result)


# Check if column are non numerical values:
#  ...if null, then the columns are valid.

# Check column -  if non-number value exist
  list_non_number_val_in_col(df$Open)
  
  non_num_list <- list_non_number_val_in_col(df$Open)
  
  # Deleting rows that contain non-number value
  df <- df[-non_num_list,]
  
  list_non_number_val_in_col(df$Open)
  
  list_non_number_val_in_col(df$High)
  list_non_number_val_in_col(df$Low)
  list_non_number_val_in_col(df$Close)

    
#-----------------------------------------
# REFORMAT the date to ISO standard
df$Date <- as.Date(df$Date, format= "%d/%m/%Y")
  
# Modify the directory, where you want to save the file
save_to_directory = "D:\\College - Year 4\\Sem 2\\Data Mining\\Team Project\\penuel_repo\\Modified Data\\iseq_2008-2018_v2.csv"

write.csv(df, file=save_to_directory, row.names = FALSE)

