#===============================
# Sentiment Analysis - Part 2
#==============================

source("https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/R/our_functions.r")

afinn_data_source <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/Modified%20Data/Sentiment%20Data/afinn_sentiment.csv"
afinn_sent <- read.csv(afinn_data_source , sep = ",")

bing_data_source <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/Modified%20Data/Sentiment%20Data/bing_sentiment.csv"
bing_sent <- read.csv(bing_data_source , sep = ",")

nrc_data_source <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/Modified%20Data/Sentiment%20Data/nrc_sentiment.csv"
nrc_sent <- read.csv(nrc_data_source , sep = ",")
nrc_raw <- nrc_sent

#----------------------------------
#
# NRC Sentiment:
# 1. find the the words that ccontains positive and negatiev on Sentiment Category...
# 2. ... and seperate them into a new dataframe

# 1. find the the words that contains positive and negative on Sentiment Category...
#
  nrc_positive <- nrc_sent[which(nrc_sent$Sentiment_Category == "positive"), ]
  nrc_negative <- nrc_sent[which(nrc_sent$Sentiment_Category == "negative"), ]
  
  # Seperate to new dataframe
  #
  nrc_sent_v2 <- rbind(nrc_positive, nrc_negative)
  colnames(nrc_sent_v2)[2] <- "Sentiment"
  
  # Remove rows which contains positive and negative
  nrc_sent<-nrc_sent[!(nrc_sent$Sentiment_Category=="positive"),]
  nrc_sent<-nrc_sent[!(nrc_sent$Sentiment_Category=="negative"),]
 
  
#-------------------
#   afinn_sent
#
  # Seperates word with score sentiment score over 0 to positive
  afinn_pos <- afinn_sent[which(afinn_sent$Sentiment_Score > 0), ]
  afinn_pos$Sentiment <- "positive"
  afinn_pos <- afinn_pos[,-2]
  
  # Seperates word with score sentiment score over 0 to negative
  afinn_neg <- afinn_sent[which(afinn_sent$Sentiment_Score < 0), ]
  afinn_neg$Sentiment <- "negative"
  afinn_neg <- afinn_neg[,-2]
  
  afinn_sent_v2 <- rbind(afinn_pos, afinn_neg)
  afinn_sent_v2$Sentiment <- as.factor(afinn_sent_v2$Sentiment)

  
  # Find row index where affin_sent$Word != bing_sent$Word vice versa
  #
  
 
  install.packages("sqldf")
  library(sqldf)
  
  word_sentiment <- afinn_sent_v2

  sent_x1 <- sqldf('SELECT Word FROM bing_sent EXCEPT SELECT Word FROM word_sentiment')
  
  dfx3 <- data.frame()
  for(word in sent_x1$Word){
    print(paste("word: ",word))
    dfx2 <- bing_sent[which(bing_sent$Word == word), ]
    dfx3 <- rbind(dfx3, dfx2)
  }
  
  word_sentiment <- rbind(word_sentiment, dfx3)  
  
  
  sent_x1 <- sqldf('SELECT Word FROM nrc_sent_v2 EXCEPT SELECT Word FROM word_sentiment')
  
  dfx3 <- data.frame()
  for(word in sent_x1$Word){
    print(paste("word: ",word))
    dfx2 <- nrc_sent_v2[which(nrc_sent_v2$Word == word), ]
    dfx3 <- rbind(dfx3, dfx2)
  }
  
  word_sentiment <- rbind(word_sentiment, dfx3)  

  dir_filename <- "D:\\College - Year 4\\Sem 2\\Data Mining\\Team Project\\penuel_repo\\Modified Data\\word_sentiment.csv"
  write.csv(word_sentiment, dir_filename, row.names = FALSE)
  