#
# https://www.tidytextmining.com/sentiment.html 
#
#   Sentiment Analyisis - Part 1 : Data Cleansing Process

source("https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/R/our_functions.r")

#-----------------------------------------------------------------------------------------------
# A FINN Sentiment Score
#   Informatics and Mathematical Modelling, Technical University of Denmark - March 2011
#   Link:  http://www2.imm.dtu.dk/pubdb/views/publication_details.php?id=6010
#
#------------------------------------------------------------------------------------------------------

  install.packages("tidytext")
  
  # PackageSource:  https://rdrr.io/cran/textdata/ 
  install.packages("textdata")
  
  library(tidytext)
  
  afinn_sent <- get_sentiments("afinn")
  
  colnames(afinn_sent)[1] <- "Word"
  colnames(afinn_sent)[2] <- "Sentiment_Score"

  # Saving as csv file: 
  save_dir_filename <- "D:\\College - Year 4\\Sem 2\\Data Mining\\Team Project\\penuel_repo\\Modified Data\\Sentiment Data\\afinn_sentiment.csv"
  write.csv(afinn_sent, save_dir_filename, row.names = FALSE)

#---------------------------------------------------------
#   NRC from Saif Mohammad and Peter Turney
# http://saifmohammad.com/WebPages/NRC-Emotion-Lexicon.htm
#  
#   Citation:
#     Crowdsourcing a Word-Emotion Association Lexicon, 
#       -- Saif Mohammad and Peter Turney, Computational Intelligence, 29 (3), 436-465, 2013.
#     Emotions Evoked by Common Words and Phrases: Using Mechanical Turk to Create an Emotion Lexicon
#        -- Saif Mohammad and Peter Turney, In Proceedings of the NAACL-HLT 2010 Workshop on Computational Approaches to Analysis and Generation of Emotion in Text, June 2010, LA, California.
#
#---------------------------------------------------------

  nrc_sent <- get_sentiments("nrc")
  
  colnames(nrc_sent)[1] <- "Word"
  colnames(nrc_sent)[2] <- "Sentiment_Category"
  
  # Saving as csv file:
  save_dir_filename <- "D:\\College - Year 4\\Sem 2\\Data Mining\\Team Project\\penuel_repo\\Modified Data\\Sentiment Data\\nrc_sentiment.csv"
  write.csv(nrc_sent, save_dir_filename, row.names = FALSE)
  
  

#----------------------------------
#  Bing Lui and collab
# ( https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html)
  #
# - Hao Wang, Bing Liu, Chaozhuo Li, Yan Yang, and Tianrui Li. "Learning with Noisy Labels for Sentence-level Sentiment Classification." to appear in Proceedings of 2019 Conference on Empirical Methods in Natural Language Processing (EMNLP-2019, short paper), Nov 3-7, 2019, Hong Kong, China.
# - Huaishao Luo, Tianrui Li, Bing Liu, and Junbo Zhang. DOER: Dual Cross-Shared RNN for Aspect Term-Polarity Co-Extraction. to appear in Proceedings of Annual Meeting of the Association for Computational Linguistics (ACL-2019), July 28 - August 2, 2019, Florence, Italy.
# - Hu Xu, Bing Liu, Lei Shu and Philip S. Yu. BERT Post-Training for Review Reading Comprehension and Aspect-based Sentiment Analysis. Proceedings of the 2019 Annual Conference of the North American Chapter of the Association for Computational Linguistics (NAACL-2019). June 2-7, 2019. Minneapolis, USA. 
# 
#-----------------------------------
  
  # IMPORTING a list of positive words
    data_source <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/Modified%20Data/Sentiment%20Data/Bing%20Lui%20and%20collab/positive-words.txt"
    positive_words <- read.csv(data_source, header = FALSE)
    
  # IMPORTING a list of negative words
    data_source <- "https://gitlab.com/penmaypa/data_web_mining_team/-/raw/master/Modified%20Data/Sentiment%20Data/Bing%20Lui%20and%20collab/negative-words.txt"
    negative_words <- read.csv(data_source, header = FALSE)
  
  # Adding a column with their sentiment
  colnames(positive_words) <- "Word"
  positive_words$Sentiment <- "Positive"

  # Adding a column with their sentiment
  colnames(negative_words) <- "Word"
  negative_words$Sentiment <- "Negative"
  
  # Combining both dataset of Positive word and Negative Word
  bing_sent <- rbind(positive_words, negative_words)
  bing_sent <- bing_sent[order(bing_sent[c("Word")],decreasing = FALSE), ]

  # Exporting it as a new csv file:
  save_dir_filename <- "D:\\College - Year 4\\Sem 2\\Data Mining\\Team Project\\penuel_repo\\Modified Data\\Sentiment Data\\bing_sentiment.csv"
  write.csv(bing_sent, save_dir_filename, row.names = FALSE)
  
  
  