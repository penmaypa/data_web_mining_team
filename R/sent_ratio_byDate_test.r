#
#  CALCULATING RATIO SENTIMENT PER DAY   
#
#==============
 source("https://github.com/penmaypa/penuel-library/raw/master/R/our_functions.r")


#=============
# TEST DATA

xdf <- read.csv(
        "https://github.com/ORP-Data-Mining-team/penuel_repo/raw/master/Modified%20Data/Sample/ie_headlines_add_date.csv",
        sep=","
      )

names(xdf)

# Add colummn with value 1
xdf["count"] <- 1

colname_date <- "real_date" 
colname_xvalue <- "count"


#==============
# PARAMETERS

df <- xdf
colname_date <- colname_date
colname_xvalue <- colname_xvalue


# If dates are similar, add it together

dfx2 <-data.frame()

dfx2[1,colname_date]<-c(" ")
dfx2[1, colname_xvalue] <-c(" ")


nxa <- 1
nxb <- 1
nxd <- nxa+1
for(item in df[,1]){
  
  message("st-3: ", df[nxa,colname_date], " | ", dfx2[nxb, colname_date] )
  message("st-3-1: ", "nxa=",nxa, " nxb=",nxb)
  
  if(as.character( df[nxa,colname_date] ) == dfx2[nxb, colname_date]){
    
    message("st-1: df1$date is equals to df2$date:  ", df[nxa,colname_date]," = ", dfx2[nxb,colname_date])
    
    dfx2[nxb, colname_xvalue] <- as.integer( dfx2[nxb, colname_xvalue] ) + as.integer( df[nxa,colname_xvalue])
    
    message("st-5: dx2 value was added: ", dfx2[nxb, colname_xvalue])
    nxa <- nxa + 1
  }else{
    message("st-2: df1$date is NOT equals to df2$date:  ", df[nxa,colname_date]," = ", dfx2[nxb,colname_date])
    
    nxb <- nxb + 1
    
    if( nxa == 1){
      nxb <- nxb - 1
    }
    
    dfx2[nxb, colname_date] <- as.character( df[nxa, colname_date] )
    dfx2[nxb, colname_xvalue] <- df[nxa, colname_xvalue]
    dfx2[(nxb+1), colname_date] <- c(" ")
    
    #message("st-4: Length() = ", length(dfx2[,1]))
    
    nxa <- nxa + 1
    
  }
   
  message("\n loop-", (nxa-1), " ends \n")
}

returnx <- dfx2
