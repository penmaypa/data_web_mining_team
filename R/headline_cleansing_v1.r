#
#  SELECTING DATA from 2008 to 2018
#

source("https://github.com/penmaypa/penuel-library/raw/master/R/our_functions.r")

data_source <-''
data_source = "https://github.com/ORP-Data-Mining-team/penuel_repo/raw/master/Modified%20Data/ireland_headlines_v2.csv"
data <- read.csv(data_source, sep=",")


headlines <- select_date_range(data, "real_date", "2008-01-01","2018-12-33")

name_dir <- "/home/penmaypa/Desktop/My Repo/penuel_repo/Modified Data/ireland_headlines_2008-2018.csv)"
write.csv(headlines, name_dir, row.names=FALSE)

