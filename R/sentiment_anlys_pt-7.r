#
#  CALCULATING RATIO SENTIMENT PER DAY   
#
#==============
source("https://github.com/penmaypa/penuel-library/raw/master/R/our_functions.r")

check_package("tidyverse")
library(tidyverse)

#----------------
# CAT: Business (2018 to 2013)
  #
    datasource <- "https://github.com/ORP-Data-Mining-team/penuel_repo/raw/master/Modified%20Data/headline_5y_biz_v1.csv"
    
    df <- read.csv(datasource, sep=",")
    
    df_positive <- find_row_where_value(df,"sentiment", "Positive")
    df_positive["Positive"] <- 1
    df_positive["Negative"] <- 0
    
    df_negative<- find_row_where_value(df,"sentiment", "Negative")
    df_negative["Positive"] <- 0
    df_negative["Negative"] <- 1
    
    dfx <- rbind(df_positive, df_negative)
    
    dfx <- dfx %>% arrange(dfx[,"real_date"])
    
    
    #==============
    # PARAMETERS
    
    df <- dfx
    
    #-----------
    df <- delete_column(df,c("id","publish_date","headline_category","headline_text","sentiment"))
    
    colname_date <- "real_date" 
    #=========
    
    colname_date <- colname_date
    col_positive <- "Positive"
    col_negative <- "Negative"
    
    df["Pos_%"] <- ""
    df["Neg_%"] <- ""
    # If dates are similar, add it together
    dfx2 <-duplicate_dataframe_column(df)
    dfxb <- dfx2
    
    nxa <- 1
    nxb <- 1
    nxd <- nxa+1
    while(nxa <= nrow(df)){
      
      seldate <- df[nxa,colname_date]
      
      dategroup <- find_row_where_value(df,colname_date, as.character(seldate))
      
      pos <- sum(dategroup[,col_positive])
      neg <- sum(dategroup[,col_negative])
      pos_pc <- (pos/(pos+neg))
      pos_pc <- format(round(pos_pc, 2), nsmall = 2)
      
      neg_pc <-(neg/(pos+neg))
      neg_pc <- format(round(neg_pc, 2), nsmall = 2)
        
      de<-data.frame(seldate,pos, neg, pos_pc, neg_pc)
      names(de)<-c(colname_date,col_positive,col_negative, "Pos_%","Neg_%")
      
      
      dfx2<- rbind(dfx2, de)
      
      nxa <- nxa + nrow(dategroup)
      nxb <- nxb + 1
      
      progress_message(nxa,nrow(df))
    }
    
    returnx <- dfx2
    
    
    dir_file <- "D:\\College - Year 4\\Sem 2\\Data Mining\\Team Project\\penuel_repo\\Modified Data\\headline_5y_biz_v2.csv"
    write.csv(returnx, dir_file, row.names = FALSE)
  #
#=====================
    
#-----------------
  #
    
    
    
  
  #
#=====================